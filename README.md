# DecoderEncoder

DecoderEncoder is app for decoding and encoding information using Caesar cipher.

## Caesar cipher

Method in which each letter in the plaintext is replaced by a letter some fixed number of positions down the alphabet.

## Usage

1. Upload text from file or type it.
2. Set key for decoding or encoding, negative number for left shift, positive number for right shift.
3. Click "Расшифровать" button. Only cyrillic symbols will be decoded.
4. If it is needed, save result text to file.