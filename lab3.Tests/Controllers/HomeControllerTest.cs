﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using lab3;
using lab3.Controllers;
using lab3.Models;

namespace lab3.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        private HomeController controller;
        private ViewResult result;

        [TestInitialize]
        public void Index()
        {
            controller = new HomeController();            
        }

        [TestMethod]
        public void DecoderViewISNotNull()
        {
            result = controller.Decoder() as ViewResult;
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void DecoderOriginalText()
        {
            TextModel model = new TextModel();
            model.OriginalContent = "qwerty";
            result = controller.Decoder(model) as ViewResult;
            model = (TextModel)result.ViewData.Model;
            Assert.AreEqual("qwerty", model.OriginalContent);
        }
        
        [TestMethod]
        public void EncoderViewISNotNull()
        {
            result = controller.Encoder() as ViewResult;
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void EncoderOriginalText()
        {
            TextModel model = new TextModel();
            model.OriginalContent = "qwerty";
            result = controller.Encoder(model) as ViewResult;
            model = (TextModel)result.ViewData.Model;
            Assert.AreEqual("qwerty", model.OriginalContent);
        }

    }
}
