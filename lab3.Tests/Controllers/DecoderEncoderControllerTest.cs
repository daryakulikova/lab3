﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using lab3;
using lab3.Controllers;

namespace lab3.Tests.Controllers
{
    [TestClass]
    public class DecoderEncoderControllerTest
    {
        private DecoderEncoderController controller;
        private string result;

        [TestInitialize]
        public void Decode()
        {
            controller = new DecoderEncoderController();
        }

        [TestMethod]
        public void DecodeResult1()
        {            
            result = controller.Decode("Срйётвднба, фэ срнхщкн кучрёпэл фжмуф!!!", 2);
            Assert.AreEqual<string>("Поздравляю, ты получил исходный текст!!!",result);
        }

        [TestMethod]
        public void DecodeResult2()
        {
            result = controller.Decode("Срйётвднба, фэ срнхщкн кучрёпэл фжмуф!!!", 35);
            Assert.AreEqual<string>("Поздравляю, ты получил исходный текст!!!", result);
        }

        [TestMethod]
        public void DecodeResult3()
        {
            result = controller.Decode("Срйётвднба, фэ срнхщкн кучрёпэл фжмуф!!!", -31);
            Assert.AreEqual<string>("Поздравляю, ты получил исходный текст!!!", result);
        }     
                      
        [TestMethod]
        public void EncodeResult1()
        {
            result = controller.Encode("Поздравляю, ты получил исходный текст!!!", 2);
            Assert.AreEqual<string>("Срйётвднба, фэ срнхщкн кучрёпэл фжмуф!!!", result);
        }

        [TestMethod]
        public void EncodeResult2()
        {
            result = controller.Encode("Поздравляю, ты получил исходный текст!!!", 35);
            Assert.AreEqual<string>("Срйётвднба, фэ срнхщкн кучрёпэл фжмуф!!!", result);
        }

        [TestMethod]
        public void EncodeResult3()
        {
            result = controller.Encode("Поздравляю, ты получил исходный текст!!!", -31);
            Assert.AreEqual<string>("Срйётвднба, фэ срнхщкн кучрёпэл фжмуф!!!", result);
        }


    }
}
