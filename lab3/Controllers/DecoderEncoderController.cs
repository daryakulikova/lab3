﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace lab3.Controllers
{
    public class DecoderEncoderController : Controller
    {
        public static string alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
        public string Decode(string s, int key)
        {
            string decodedStr = "";
            key = key % 33;

            for (int i = 0; i < s.Length; i++)
            {
                string letter = char.ToLower(s[i]).ToString();
                if (alphabet.Contains(letter))
                {
                    int temp = Convert.ToInt32(alphabet.IndexOf(letter)) - key;
                    if (temp < 0)
                    {
                        temp = alphabet.Length + temp;
                    }
                    if (temp > 32)
                    {
                        temp = temp - alphabet.Length;
                    }
                    if (char.IsLower(s[i]))
                    {
                        decodedStr += alphabet[temp];
                    }
                    else
                    {
                        decodedStr += char.ToUpper(alphabet[temp]);
                    }
                }
                else
                {
                    decodedStr += letter;
                }
            }
            return decodedStr;
        }

        public ViewResult Decode()
        {
            throw new NotImplementedException();
        }

        public string Encode(string s, int key)
        {
            string decodedStr = "";
            key = key % 33;
            for (int i = 0; i < s.Length; i++)
            {
                string letter = char.ToLower(s[i]).ToString();
                if (alphabet.Contains(letter))
                {
                    int temp = Convert.ToInt32(alphabet.IndexOf(letter)) + key;
                    if (temp > 32)
                    {
                        temp -= alphabet.Length;
                    }
                    if (temp < 0)
                    {
                        temp += alphabet.Length;
                    }
                    if (char.IsLower(s[i]))
                    {
                        decodedStr += alphabet[temp];
                    }
                    else
                    {
                        decodedStr += char.ToUpper(alphabet[temp]);
                    }
                }
                else
                {
                    decodedStr += letter;
                }
            }
            return decodedStr;
        }
    }
}
