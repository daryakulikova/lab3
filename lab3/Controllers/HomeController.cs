﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using lab3.Models;
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;


namespace lab3.Controllers
{
    public class HomeController : Controller
    {
        public TextModel text = new TextModel();
        public DecoderEncoderController controller = new DecoderEncoderController();
        public ActionResult Decoder()
        {
            
            text = TempData.Peek("Text") as TextModel;

            return View(text);
        }

        public ActionResult Encoder()
        {
            text = TempData.Peek("Text2") as TextModel;

            return View(text);
        }

        public FileStreamResult Save(string code)
        {

            text = TempData.Peek(code.Equals("decoder") ? "Text" : "Text2") as TextModel;
            var data = text.DecodedContent == null ? "" : text.DecodedContent;

            var byteArray = Encoding.Default.GetBytes(data);
            var stream = new MemoryStream(byteArray);

            //return File(stream, "text/plain", String.Format("{0}.txt", example));
            return File(stream, "text/plain", "result.txt");
        }

        public FileContentResult SaveDocx(string code)
        {
            text = TempData.Peek(code.Equals("decoder") ? "Text" : "Text2") as TextModel;
            var data = text.DecodedContent == null ? "" : text.DecodedContent;

            var ms = new MemoryStream();
            try
            {
                using (WordprocessingDocument wordDocument = WordprocessingDocument.Create(ms, WordprocessingDocumentType.Document))
                {
                    MainDocumentPart mainPart = wordDocument.AddMainDocumentPart();

                    mainPart.Document = new Document(
                        new Body(
                            new Paragraph(
                                new Run(
                                    new Text(data)))));
                }

                return File(ms.ToArray(), "application/docx", "result.docx");
                //return File(ms.ToArray(), "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "result.docx");
            }
            catch
            {
                ms.Dispose();
                throw;
            }
        }

        [HttpPost]
        public ActionResult UploadDecoder(HttpPostedFileBase upload)
        {
            if (upload != null)
            {
                text.OriginalContent = GetContent(upload);
            }

            TempData["Text"] = text;
            TempData.Keep("Text");
            return RedirectToAction("Decoder");
        }

        private string GetContent(HttpPostedFileBase upload)
        {
            string fileName = System.IO.Path.GetFileName(upload.FileName);
            string contents = "";

            if (System.IO.Path.GetExtension(upload.FileName).Equals(".docx"))
            {
                upload.InputStream.Position = 0;
                using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(upload.InputStream, false))
                {
                    Body body = wordDoc.MainDocumentPart.Document.Body;
                    foreach (var paragraph in body.Elements<Paragraph>())
                    {
                        foreach (var run in paragraph.Elements<Run>())
                        {
                            foreach (var text in run.Elements<Text>())
                            {
                                contents += text.InnerText;
                            }
                        }
                    }
                }
            }
            else
            {
                upload.InputStream.Position = 0;
                contents = new StreamReader(upload.InputStream).ReadToEnd();
            }
            return contents;
        }

        [HttpPost]
        public ActionResult UploadEncoder(HttpPostedFileBase upload)
        {
            if (upload != null)
            {
                text.OriginalContent = GetContent(upload);
            }

            TempData["Text2"] = text;
            TempData.Keep("Text2");
            return RedirectToAction("Encoder");
        }

        [HttpPost]
        public ActionResult Decoder(TextModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            ModelState.Remove("DecodedContent");
            model.DecodedContent = controller.Decode(model.OriginalContent, Convert.ToInt32(model.Key));

            TempData["Text"] = model;
            TempData.Keep("Text");
            return View(model);
        }

        [HttpPost]
        public ActionResult Encoder(TextModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            ModelState.Remove("DecodedContent");
            model.DecodedContent = controller.Encode(model.OriginalContent, Convert.ToInt32(model.Key));

            TempData["Text2"] = model;
            TempData.Keep("Text2");
            return View(model);
        }


    }
}