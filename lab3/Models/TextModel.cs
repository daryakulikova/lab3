﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace lab3.Models
{
    public class TextModel
    {
        [Required(ErrorMessage = "Поле должно быть установлено")]
        public string OriginalContent { get; set; }

        [DisplayName("Результат")]
        public string DecodedContent { get; set; }

        [Required(ErrorMessage = "Поле должно быть установлено")]
        [DisplayName("Ключ")]
        public int Key { get; set; }
    }
}